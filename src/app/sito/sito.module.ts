import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SitoPageRoutingModule } from './sito-routing.module';

import { SitoPage } from './sito.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SitoPageRoutingModule
  ],
  declarations: [SitoPage]
})
export class SitoPageModule {}
