import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SitoPage } from './sito.page';

describe('SitoPage', () => {
  let component: SitoPage;
  let fixture: ComponentFixture<SitoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SitoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SitoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
