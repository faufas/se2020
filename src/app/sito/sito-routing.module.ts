import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SitoPage } from './sito.page';

const routes: Routes = [
  {
    path: '',
    component: SitoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SitoPageRoutingModule {}
