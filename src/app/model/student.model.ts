export interface Student {
    matricola: number;
    nome: string;
    cognome: string;
    cds: string;
}
