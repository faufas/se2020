import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Student } from './student.model';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { first, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  // tslint:disable-next-line: variable-name
  private _studenti = new BehaviorSubject<Student[]>([]);

  get studenti() {
    return this._studenti.asObservable();
  }

  constructor(private storage: Storage, private http: HttpClient) {
    this.fetchFromBackend();
  }

  fetchFromBackend() {
    this.http.get('http://localhost/students').subscribe(
      response => {
        this._studenti.next(response['students']);
      }
    );
  }

  addStudent(student: Student) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    this.http.post('http://localhost/students', JSON.stringify(student), httpOptions).subscribe(
      result => {
        console.log(result);
        return this.fetchFromBackend();
      }
    );
  }

  // getAll(): Student[] {
  //   return [...this.studenti];
  // }

  // getStudent(matricola: number): Student {

  //   return [...this.studenti].find(
  //     studente => {
  //       return studente.matricola === matricola;
  //     }
  //   );
  // }

  deleteStudent(matricola: number) {
    console.log('Stai cancellando', matricola);

    return this.studenti.pipe(
      first(),
      tap(studenti => {
        this._studenti.next(
          studenti.filter(
            studente => {
              return studente.matricola !== matricola;
            }
          )
        );
      })
    );
  }

  // ionic generate page studenti/new-studente

  // findStudentByCDS(cds: string): Student[] {
  //   return [...this.studenti].filter(
  //     studente => {
  //       return studente.cds === cds;
  //     }
  //   );
  // }

  // addStudent(student: Student) {
  //   this.studenti.push(student);
  // }

  // geAllStudenteByCds(cds: string): Student[] {

  //   // Gestiamo l'entry condition (precondizioni)
  //   if (!cds || cds === '') {
  //     // return [...this.studenti];
  //     return [];
  //   }
  //   const cdsToFilter = cds.toLocaleLowerCase(); // info

  //   return [...this.studenti].filter(
  //     (item) => {
  //       try {
  //         return (item.cds.toLocaleLowerCase().indexOf(cdsToFilter) > -1);
  //         // return (item.cds.toLocaleLowerCase() === cdsToFilter);
  //       } catch (err) {
  //         console.log(err);
  //       }
  //     }
  //   );
  // }










  // Questo non c'entra niente !

  testStorage() {


    // Conversione JSON -> Object -> JSON
    const myJSON = null;
    const jsObj = JSON.parse(myJSON);
    const myJSON2 = JSON.stringify(jsObj);


    // Chiamate serializzate (una dopo l'altra!)
    this.storage.get('nome').then(
      val => {
        this.storage.get('cognome').then(
          val2 => {
            this.storage.get('esami').then(
              val3 => {
                this.storage.get('insegnamenti').then(
                  val4 => {
                    console.log(val + val2 + val3 + val4);
                    /// Qui ho letto tutto!!!
                    // JSON.parse(val4);
                  }
                );
              }
            );
          }
        );
      }
    ).catch(
      e => {
        // Gestisco eventuali eccezioni
      }
    );


    const prom1 = this.storage.get('nome');
    const prom2 = this.storage.get('cognome');
    const prom3 = this.storage.get('esami');
    const prom4 = this.storage.get('insegnamenti');

    // In parallelo !!!
    // Attendo che TUTTE le Promise siano risolte (o che qualcuno sia stata rigettata)
    Promise.all([prom1, prom2, prom3, prom4]).then(
      valori => {
        console.log(valori[0] + valori[1] + valori[2] + valori[3]);
      },
      rej => {
        // qualcosa non ha funzionato
      }
    );

    const promA = this.storage.get('nome');
    const promB = this.storage.get('cognome');
    const promC = this.storage.get('esami');
    const promD = this.storage.get('insegnamenti');

    // In parallelo !!!
    // La prima Promise risolta viene presa in considerazione
    Promise.race([promA, promB, promC, promD]).then(
      valore => {
        console.log(valore);
      },
      rej => {
        // qualcosa non ha funzionato
      }
    );


    // Codice che non serve
    this.storage.get('fausto').then(  // function (val) {// blablabla}
      val => {
        console.log('Your name is', val);
      },
      rej => {
        console.log('Reject name is', rej);
      }).catch(e => {  // function (e) { //blablabla }
        console.log(e);
      }).then(
        () => { // function() { // blablabla}
          console.log('E\' andato tutto bene!');
          // jj
        },
        () => {
          console.log('E\' andato tutto male!');
          // jj
        }
      ).catch(e => {  // function (e) { //blablabla }
        console.log(e);
      });
  }

}
