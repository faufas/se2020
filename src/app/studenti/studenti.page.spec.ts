import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StudentiPage } from './studenti.page';

describe('StudentiPage', () => {
  let component: StudentiPage;
  let fixture: ComponentFixture<StudentiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StudentiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
