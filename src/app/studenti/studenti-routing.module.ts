import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentiPage } from './studenti.page';

const routes: Routes = [
  {
    path: '',
    component: StudentiPage
  },
  {
    path: 'studente',
    loadChildren: () => import('./studente/studente.module').then( m => m.StudentePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentiPageRoutingModule {}
