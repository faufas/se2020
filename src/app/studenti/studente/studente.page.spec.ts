import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StudentePage } from './studente.page';

describe('StudentePage', () => {
  let component: StudentePage;
  let fixture: ComponentFixture<StudentePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StudentePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
