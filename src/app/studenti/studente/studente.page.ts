import { Student } from './../../model/student.model';
import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-studente',
  templateUrl: './studente.page.html',
  styleUrls: ['./studente.page.scss'],
})
export class StudentePage implements OnInit {

  @Input() student: Student;

  constructor(
    private modalController: ModalController
    ) { }

  ngOnInit() {
  }

  onClose() {
    this.modalController.dismiss(this.student, 'cancel');
  }

}
