import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentePageRoutingModule } from './studente-routing.module';

import { StudentePage } from './studente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentePageRoutingModule
  ],
  declarations: [StudentePage]
})
export class StudentePageModule {}
