import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentePage } from './studente.page';

const routes: Routes = [
  {
    path: '',
    component: StudentePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentePageRoutingModule {}
