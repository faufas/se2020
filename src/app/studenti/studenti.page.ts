import { StudentePage } from './studente/studente.page';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Student } from './../model/student.model';
import { StudentService } from './../model/student.service';
import { ModalController, IonItemSliding } from '@ionic/angular';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-studenti',
  templateUrl: './studenti.page.html',
  styleUrls: ['./studenti.page.scss'],
})
export class StudentiPage implements OnInit, OnDestroy {

  infiniteScroll: any;

  studenti: Student[];
  sezione: 'tutti' | 'informatica' = 'tutti';

  nrElementi = 0;
  nrElementiDaMostrare = 5;
  step = 5;

  students: {
    tutti: Student[],
    informatica: Student[]
  };

  private studentiSubscription: Subscription;


  constructor(
    private studentService: StudentService,
    private modalController: ModalController
  ) {
    // Inizializzaizone
  }

  ngOnInit() {
    // this.students = {
    //   tutti: this.studentService.getAll(),
    //   informatica: this.studentService.geAllStudenteByCds('info')
    // };
    // this.carica();

    this.studentiSubscription = this.studentService.studenti.subscribe(
      studenti => {
        this.students = {
          tutti: studenti,
          informatica: studenti
        };
        this.carica();
      }
    );
  }

  ngOnDestroy() {
    if (this.studentiSubscription) {
      this.studentiSubscription.unsubscribe();
    }
  }

  onAddStudent() {
    const student: Student = {
      matricola: Math.random() * 1000000 | 0,
      cds: Math.random() > 0.5 ? 'Informatica' : 'Biologia',
      nome: Math.random() > 0.5 ? 'Fausto' : 'Mario',
      cognome: Math.random() > 0.5 ? 'Fasano' : 'Rossi',
    };
    this.studentService.addStudent(student);
  }

  show(studente: Student, item: IonItemSliding) {
    if (item) {
      item.close();
    }

    console.log(studente);
    this.modalController.create(
      {
        component: StudentePage,
        componentProps: { student: studente }
      }
    ).then(
      studenteModal => {
        studenteModal.present();
        return studenteModal.onDidDismiss();
      }
    ).then(
      dismissed => {
        console.log('Che hai fatto?:', dismissed.role);
        console.log('Valore:', dismissed.data);

      }
    ).catch(ex => {
      // Oh my God!
    });
  }

  delete(studente: Student, item: IonItemSliding) {
    if (item) {
      item.close();
    }

    console.log('delete');

    this.studentService.deleteStudent(studente.matricola).subscribe();

    // this.students = {
    //   tutti: this.studentService.getAll(),
    //   informatica: this.studentService.geAllStudenteByCds('info')
    // };
    // this.carica();
  }

  onViewChange(event: CustomEvent) {
    // const el: IonSegment = event.target;
    // console.log(event);
    // console.log(el);
    // console.log('Ora il segment è', event.detail.value);

    this.carica();
  }

  carica() {
    // if (this.sezione === 'informatica') {
    //   // filtriamo
    //   this.studenti = this.studentService.geAllStudenteByCds('informatica');
    // } else {
    //   this.studenti = this.studentService.getAll();
    // }

    this.nrElementi = this.students[this.sezione].length;

    if (this.nrElementiDaMostrare > this.nrElementi) {
      this.nrElementiDaMostrare = this.nrElementi;
    }

    this.nrElementiDaMostrare = this.nrElementi;

    if (this.nrElementi > this.nrElementiDaMostrare) {
      this.studenti = this.students[this.sezione].slice(0, this.nrElementiDaMostrare);
    } else {
      this.studenti = this.students[this.sezione];
    }

    console.log('Sto mostrando', this.nrElementiDaMostrare, 'studenti su', this.nrElementi);

  }

  loadData(event: CustomEvent) {
    setTimeout(() => {
      console.log(event);
      this.infiniteScroll = event.target;

      this.nrElementiDaMostrare += this.step;
      this.carica();
      this.infiniteScroll.complete();

      //event.target.complete();

      // // App logic to determine if all data is loaded
      // // and disable the infinite scroll
      // if (data.length == 1000) {
      //   event.target.disabled = true;
      // }
    }, 500);
  }

}
