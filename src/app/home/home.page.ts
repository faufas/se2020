// import { map } from 'rxjs';

import { Component, OnInit, OnDestroy, DoCheck} from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy, DoCheck {

  nome: string;
  cognome = 'Fasano';
  mostra = true;

  elementi: string[] = ['Pippo', 'Pluto'];

  constructor(
    private router: Router,
    private http: HttpClient,
    private toast: ToastController
  ) {
    // codice mio
  }

  ngDoCheck() {
    console.log('ngDoCheck');
  }

  ngOnInit() {
    // Ciao mondo
    this.nome = 'Fausto';
    this.elementi = [];
    // this.elementi.push('Paperino');
    // this.elementi.push('Zio Paperone');
    // this.elementi.push('Nonna Papera');

    // this.elementi = null;

    this.http.get<any>('https://swapi.co/api/people/').subscribe(
      risposta => {
        console.log(risposta);
        const risultati = risposta.results;
        for (const personaggio of risultati) {
          // console.log(personaggio);
          console.log(personaggio.name + ' - ' + personaggio.hair_color);
          this.elementi.push(personaggio.name);
        }

      });
  }

  ngOnDestroy() {

  }

  public vai( indirizzo: string, direzione: number): string {
    /*
    jshfkjsdhfd
    sdfdsfsd
    */
    this.router.navigate([indirizzo]);
    return '';
  }

  public onButtonPressed() {
    this.router.navigate(['/sito']);
  }

  onCambia() {
    if (this.nome === 'Fausto') {
      this.nome = 'Stefano';
      this.cognome = 'Rossi';
    } else {
      this.nome = 'Fausto';
      this.cognome = 'Fasano';
    }
  }

  onLogin(nome: any) {
    console.log('Il cognome adesso è: ' + this.cognome);

    this.nome = nome.value;
  }

  onChange(nome: any) {
    console.log(nome.value);
  }

  showMe(): boolean {
    console.log(this.elementi.length);
    return (this.elementi &&
      (this.elementi.length > 0));
  }

  testFor() {
    const nrElementi = this.elementi.length;
    for ( let i = 0; i < nrElementi; i++ ) {
       const elemento = this.elementi[i];
       console.log(elemento);
    }

    for (const elemento of this.elementi) {
      console.log(elemento);
    }
  }
}
