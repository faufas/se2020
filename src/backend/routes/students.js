const express = require("express");
const studentModel = require("../models/studentsModel");

const router = express.Router();

router.get("/", (req, res, next) => {
    res.status(200).json({
        students: studentModel.students,
    });
});

router.get("/:matricola", (req, res, next) => {
    const matricola = req.params.matricola;
    const student = studentModel.getStudent(matricola);

    if (student) {
        res.status(200).json({
            message: "Studente trovato!",
            student: student,
        });
    } else {
        res.status(404).json({
            message: "Studente non trovato!",
        })
    }

});

router.post("/", (req, res, next) => {

    const student = {
        matricola: req.body.matricola,
        nome: req.body.nome,
        cognome: req.body.cognome,
        cds: req.body.cds
    }

    // Inserisco nel Model
    studentModel.addStudent(student);

    res.status(200).json({
        message: "Studente creato correttamente",
        studente: student,
    });
});


router.delete("/:matricola", (req, res, next) => {
    const matricola = req.params.matricola;

    res.status(200).json({
        message: "Hai eliminato la matricola " + matricola,
    });
});

module.exports = router;