let students = [{
    matricola: 123456,
    nome: "Roberto",
    cognome: "Baggio",
    cds: "Informatica",
  },
  {
    matricola: 123457,
    nome: "Paolo",
    cognome: "Rossi",
    cds: "Informatica",
  },
  {
    matricola: 123458,
    nome: "Paolo",
    cognome: "Maldini",
    cds: "Biologia",
  },
  {
    matricola: 123459,
    nome: "Lionel",
    cognome: "Messi",
    cds: "Informatica",
  },
  {
    matricola: 123460,
    nome: "Diego Armando",
    cognome: "Maradona",
    cds: "Biologia",
  },
  {
    matricola: 123461,
    nome: "Dino",
    cognome: "Zoff",
    cds: "Biologia",
  },
  {
    matricola: 123462,
    nome: "Gigio",
    cognome: "Donnarumma",
    cds: "Informatica",
  },
  {
    matricola: 123463,
    nome: "Zlatan",
    cognome: "Ibrahimović",
    cds: "Medicina",
  },
  {
    matricola: 123464,
    nome: "Giacomo",
    cognome: "Bonaventura",
    cds: "Giurisprudenza",
  },
  {
    matricola: 123465,
    nome: "Alessandro Nesta",
    cognome: "Maldini",
    cds: "Biologia",
  },
  {
    matricola: 123466,
    nome: "Gianluigi",
    cognome: "Buffon",
    cds: "Medicina",
  },
  {
    matricola: 123467,
    nome: "Fabio",
    cognome: "Grosso",
    cds: "Biologia",
  },
  {
    matricola: 123468,
    nome: "Marco",
    cognome: "Materazzi",
    cds: "Biologia",
  },
  {
    matricola: 123469,
    nome: "Alessandro",
    cognome: "Del Piero",
    cds: "Informatica",
  },
  {
    matricola: 123470,
    nome: "Filippo",
    cognome: "Inzaghi",
    cds: "Informatica",
  },
  {
    matricola: 123471,
    nome: "Giorgio",
    cognome: "Chiellini",
    cds: "Biologia",
  },
  {
    matricola: 123473,
    nome: "Andrea",
    cognome: "Pirlo",
    cds: "Informatica",
  },
  {
    matricola: 123474,
    nome: "Francesco",
    cognome: "Totti",
    cds: "Informatica",
  },
  {
    matricola: 123475,
    nome: "Roberto",
    cognome: "Mancini",
    cds: "Biologia",
  },
  {
    matricola: 123476,
    nome: "Carlo",
    cognome: "Ancelotti",
    cds: "Informatica",
  },
  {
    matricola: 123477,
    nome: "Ciro",
    cognome: "Ferrara",
    cds: "Biologia",
  },
];

function getStudent(matricola) {
  return students.find(
    student => {
      return student.matricola === +matricola;
    },
  );
}

function addStudent(student) {
  const exists = getStudent(student.matricola);
  if (exists) {
    throw new Error("Matricola già presente!");
  } else {
    students.push(student);
  }
}


exports.students = students;
exports.getStudent = getStudent;
exports.addStudent = addStudent;

