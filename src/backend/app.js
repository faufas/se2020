const express = require("express");
const studentsRoutes = require("./routes/students");
const bodyParser = require("body-parser");

const app = express();

app.use(
    (req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "*");
        next();
    },
);

app.use( bodyParser.urlencoded({extended: false}) );
app.use( bodyParser.json() );

app.use("/students", studentsRoutes);

app.use(
    (req, res, next) => {
        const error = new Error("Endpoint non supportato!");
        error.status = 405;
        next(error);
    },
);

app.use(
    (error, req, res, next) => {
        res.status(error.status || 500);
        res.json({
            error: {
                message: error.message,
            },
        });
    });

module.exports = app;
